This is a cloud-init script to set up a `Redmine`_ appliance.

See also:
 - You can use `Downburst`_ to create an appliance.
 - This script is based on `Redmine's documentation`_.

.. _`Redmine's documentation`: http://www.redmine.org/projects/redmine/wiki/HowTo_Install_Redmine_on_Ubuntu_step_by_step
.. _`Redmine`: http://www.redmine.org/
.. _`Downburst`: https://github.com/ceph/downburst

